// #include "scenes/StartScene.h"
#include "Animation.h"
#include "AssetManager.h"
#include "Game.h"
#include "SceneManager.h"
#include "scenes/StartScene.h"
#include <boost/ut.hpp>
#include <iostream>
#include <memory>
#include <standalone/fakeit.hpp>
#include <string_view>

int main()
{
  using namespace boost::ut;
  using namespace fakeit;

  try {

    "Animation"_test = [] {
      netgame::AnimationDescription animation_desc;
      animation_desc.m_cellsize = { 64, 64 };
      animation_desc.startpos = { 0, 64 * 8 };
      animation_desc.n_frames = 8;
      animation_desc.duration = 1.2f;
      // netgame::Animation animation{animation_desc};

      expect(animation_desc.m_spritesheet == nullptr);
    };
    // "Scene"_test = [] {
    //   Mock<netgame::Game> game;
    //   netgame::NewAssetManager assets{ "" };
    //   When(Method(game, get_assets)).Return(assets);
    //   auto scene = netgame::StartScene{};
    //   expect(scene.m_game == nullptr);
    //   scene.init(std::make_shared<netgame::Game>(game));
    //   expect(scene.m_game != nullptr);
    //   expect(!scene.exit);
    // };
  } catch (const std::exception &e) {
    std::cout << e.what() << '\n';
  } catch (...) {
    std::cout << "eRROROOROR" << '\n';
  }
}
