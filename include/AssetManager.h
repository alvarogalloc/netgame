#pragma once
#include <future>
#include <iostream>
#include <memory>
#include <span>
#include <string_view>
#include <type_traits>
#include <filesystem>
#include <unordered_map>

#include "Exceptions.h"
#include <SFML/Audio/Music.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <spdlog/spdlog.h>

namespace netgame {

// template<typename T, typename... U>
// concept any_of = (std::same_as<T, U> || ...);
//
class AssetManager
{
  template<typename T> using ptr = std::shared_ptr<T>;
  template<typename T>
  using hash_map = std::unordered_map<std::string_view, ptr<T>>;

private:
  template<typename T>
  void load(std::string_view path, hash_map<T> &target_map, bool load = true)
  {
    spdlog::info("Loading {}", path);
    bool error = false;
    // get type of the map
    using insert_type =
      std::remove_cvref_t<decltype(target_map)>::mapped_type::element_type;
    auto [res, ok] =
      target_map.insert({ path, std::make_shared<insert_type>() });
    error = !ok;
    if (load)
    {
      if constexpr (std::is_same_v<insert_type, sf::Music>)
        error = !res->second->openFromFile(asset_path + path.data());
      else
        error = !res->second->loadFromFile(asset_path + path.data());
    }
    // error will close the game
    if (error) throw netgame::resource_error{ path };
    spdlog::info("Success loading {}", path);
  }

public:
  explicit AssetManager(std::string_view _asset_path);

  template<typename T>
  [[nodiscard]] ptr<T> get(std::string_view path)
  {
    static_assert(std::is_same_v<T, sf::Texture> || std::is_same_v<T, sf::Music>
                    || std::is_same_v<T, sf::Font>,
      "Asset Type is not supported");
    // get container to pass to "load"
    hash_map<T> *container{ nullptr };
    if constexpr (std::is_same_v<T, sf::Music>)
      container = &songs;
    else if constexpr (std::is_same_v<T, sf::Font>)
      container = &fonts;
    else
      // else if constexpr (std::is_same_v<T, sf::Texture>)
      container = &textures;
    if (!container->contains(path))
      load(path, *container);
    else
      spdlog::info("Asset \"{}\" loaded, returning...", path);
    return container->at(path);
  }
  template<typename T>
  [[nodiscard]] std::future<ptr<T>> async_get(std::string_view path)
  {
    return std::async(std::launch::async, &AssetManager::get<T>, this, path);
  }

private:
  std::string asset_path;
  hash_map<sf::Texture> textures;
  hash_map<sf::Music> songs;
  hash_map<sf::Font> fonts;
};
}// namespace netgame
