#pragma once
#include "Animation.h"
#include <entt/entity/registry.hpp>
#include <functional>

namespace netgame {
class Game;
using entity = entt::registry::entity_type;
namespace component {
  struct no_render
  {
  };
}// namespace component


template<typename Type> class hook;

template<typename Ret, typename... Args> class hook<Ret(Args...)>
{
private:
  using callback_type = std::function<Ret(Args...)>;
  std::vector<callback_type> m_callbacks;

public:
  void connect(callback_type callback, bool front = false)
  {
    if (front)
      m_callbacks.insert(m_callbacks.begin(), callback);
    else
      m_callbacks.push_back(callback);
  }

  void publish(Args... args)
  {
    for (auto callback : m_callbacks) callback(args...);
  }
};

// our gameloop has to have 4 stages
struct GameState
{
  GameState(Game *);
  entt::registry db;
  std::vector<std::function<void(entt::registry &)>> setup_hooks;
  std::vector<std::function<void(entt::registry &, float delta)>> update_hooks;
  std::vector<std::function<void(entt::registry &)>> end_hooks;

  inline entity add_entity() { return db.create(); };
  void update(float delta);
  void render(sf::RenderTarget &target);
  inline void delete_entity(entity id) { db.destroy(id); }
};

}// namespace netgame
