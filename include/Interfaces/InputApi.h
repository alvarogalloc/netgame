#pragma once
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>

namespace netgame {


class InputApi// NOLINT
{
public:
  [[nodiscard]] virtual bool is_key_pressed(sf::Keyboard::Key key) const = 0;
  [[nodiscard]] virtual bool is_key_released(sf::Keyboard::Key key) const = 0;
  [[nodiscard]] virtual bool is_key_down(sf::Keyboard::Key key) const = 0;
  [[nodiscard]] virtual bool is_mouse_pressed(
    sf::Mouse::Button button) const = 0;
  [[nodiscard]] virtual bool is_mouse_released(
    sf::Mouse::Button button) const = 0;
  [[nodiscard]] virtual bool is_mouse_down(sf::Mouse::Button button) const = 0;
  [[nodiscard]] virtual sf::Vector2i get_mouse_position() const = 0;
  virtual ~InputApi() = default;
};
}// namespace netgame
