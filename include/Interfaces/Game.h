#pragma once
#include "AssetManager.h"
#include "GameState.h"
#include "SceneManager.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <span>
#include <string_view>
#include "Interfaces/InputApi.h"
namespace netgame {
class GameState;
class Scene;

class Game // NOLINT
{
public:
  [[nodiscard]] virtual bool running() const = 0;
  virtual void update() = 0;
  virtual void render() = 0;
  [[nodiscard]] virtual sf::RenderWindow &get_window() = 0;
  [[nodiscard]] virtual AssetManager &get_asset_manager() = 0;
  [[nodiscard]] virtual SceneManager &get_scene_manager() = 0;
  [[nodiscard]] virtual sf::Clock &get_clock() = 0;
  [[nodiscard]] virtual sf::VideoMode get_dimensions() const = 0;
  [[nodiscard]] virtual GameState &get_state() = 0;
  [[nodiscard]] virtual InputApi &get_input_api() = 0;

  virtual ~Game() = default;
};
}// namespace netgame
