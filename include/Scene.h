#pragma once
#include <memory>
namespace sf {
class RenderTarget;
class Event;
}// namespace sf

namespace netgame {
class AssetManager;
class Game;
class GameState;
class Scene
{
public:

  virtual ~Scene() = default;
  using next = Scene;
  void init(Game *game);
  virtual void on_init() = 0;
  virtual void on_update(float) = 0;
  virtual void on_render(sf::RenderTarget &) = 0;
  virtual void on_event(sf::Event &) = 0;
  virtual std::unique_ptr<Scene> on_exit() = 0;


  AssetManager *m_assets{ nullptr };
  bool exit{ false };
  Game *m_game{ nullptr };
  GameState *m_state{ nullptr };
};

}// namespace netgame
