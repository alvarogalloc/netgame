#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <vector>

namespace netgame {

// make a new animation, taking only the spritesheet, the number of rows and
// columns, and the duration of the animation, and a vector of the sf::Vector2i
// of the start and end frames of each animation

// For now only support for animations with the same size
// and in horizontal order
class Animator : public sf::Drawable
{
public:
  Animator() = default;
  Animator(sf::Texture *spritesheet, sf::Vector2u tileset_size);
  // add all other constructors
  Animator(const Animator &) = default;
  Animator(Animator &&) = default;
  Animator &operator=(const Animator &) = default;
  Animator &operator=(Animator &&) = default;
  // add a new animation to the animator
  // start_frame and end_frame are the coordinates of the top left corner of the
  // frame and the bottom right corner of the frame respectively

  void add_animation(std::string_view name,
    sf::Vector2u start_frame,
    sf::Vector2u end_frame,
    float duration,
    bool repeat);
  // get the current frame of the animation
  [[nodiscard]] inline auto get_sprite() -> sf::Sprite & { return m_sprite; }
  // get the frame at a specific time
  // auto get_sprite(float delta) -> sf::Sprite & { return m_sprite; };
  inline void pause() { m_paused = true; }
  inline bool playing() const { return !m_paused;}
  void play(std::string_view name);
  void update(float delta);
  inline void draw(sf::RenderTarget &target, sf::RenderStates states) const override
  {
    target.draw(this->m_sprite, states);
  }

private:
  void set_frame(const sf::Vector2u frame);
  void replay();
  struct AnimationDescription
  {
    std::string_view name;
    sf::Vector2u start_frame;
    sf::Vector2u end_frame;
    float duration;
    bool repeat;
  };
  // non-owning pointer to the spritesheet
  sf::Texture *m_spritesheet{};
  // n rows and columns in the spritesheet
  sf::Vector2u m_tileset_size{ 0, 0 };
  std::vector<AnimationDescription> m_animations;
  std::vector<AnimationDescription>::iterator m_curr_animation;
  sf::Vector2u m_curr_frame;
  float m_progress{ 0 };

  sf::Sprite m_sprite;
  bool m_paused{ true };
};

namespace old {

  struct AnimationDescription
  {
    sf::Texture *m_spritesheet{};
    // offset in x and y from the top left corner of the sprite
    // and the size of the sprite
    sf::IntRect pos_size;
    uint8_t n_frames{ 0 };
    uint8_t curr_frame{ 0 };
    float duration{ 0 };
    float progress{ 0 };
    bool repeat{ true };
  };
  // TODO: add support for rows and columns and different directions

  class Animation : public sf::Drawable
  {
  private:
    AnimationDescription m_description;
    sf::Sprite m_sprite;
    bool m_flipped{ false };
    bool m_is_playing{ false };
    void reset();
    void nextFrame();

  public:
    Animation(const Animation &) = default;
    Animation(Animation &&) = default;
    Animation &operator=(const Animation &) = default;
    Animation &operator=(Animation &&) = default;
    Animation() = default;
    explicit Animation(AnimationDescription description);
    void update(float delta);
    void set_flipped(bool is_flip);
    [[nodiscard]] sf::Sprite &get_sprite() { return m_sprite; }
    void play() { m_is_playing = true; }
    void stop() { m_is_playing = false; }
    inline void draw(sf::RenderTarget &target, sf::RenderStates states) const override
    {
      target.draw(this->m_sprite, states);
    }
    ~Animation() override = default;
  };
}// namespace old
}// namespace netgame
