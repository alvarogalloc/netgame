#pragma once
#include "Animation.h"
#include "Scene.h"
#include <SFML/Audio/Music.hpp>
#include <SFML/Graphics/Text.hpp>
#include <memory>

namespace netgame {

class StartScene final : public Scene
{
public:
  void on_init() override;
  void on_update(float delta) override;
  void on_event(sf::Event &ev) override;
  void on_render(sf::RenderTarget &gfx) override;
  std::unique_ptr<Scene> on_exit() override;
  ~StartScene() override = default;

private:
  Animator m_capy_walk;
  sf::Sprite m_bg_sprite;
  sf::Sprite m_capy_debug_sprite;
  std::shared_ptr<sf::Music> music;
  sf::Sprite m_play_button;
  bool is_blending_up{ false };
  std::unique_ptr<sf::Text> m_title_text;
  std::unique_ptr<sf::Text> m_start_text;
};
}// namespace netgame
