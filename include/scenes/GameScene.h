#pragma once
#include "Scene.h"
#include "GameState.h"
#include "Tilemap.h"
#include <SFML/Graphics/Image.hpp>

namespace netgame {
class StartScene;

class GameScene final : public netgame::Scene
{
private:
  entity m_player;
  Tilemap m_map;
public:
  using next = StartScene;
  void on_init() override;
  void on_update(float delta) override;
  void on_event(sf::Event &ev) override;
  void on_render(sf::RenderTarget &gfx) override;
  std::unique_ptr<Scene> on_exit() override;
};
}// namespace netgame
