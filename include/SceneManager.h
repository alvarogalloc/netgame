#pragma once
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Event.hpp>
#include <Scene.h>
// #include <cassert>
#include <memory>
#include <utility>

namespace netgame {
class Game;
struct SceneManager
{
  explicit SceneManager(Game *game, std::unique_ptr<Scene> first_scene)
    : m_game(game), m_current_scene(std::move(first_scene))
  {
    m_current_scene->init(m_game);
  }
  void init() { m_current_scene->on_init(); }
  ~SceneManager() = default;
  void change_scene()
  {
    m_current_scene = m_current_scene->on_exit();
    m_current_scene->init(m_game);
    m_current_scene->on_init();
  }
  void update(float delta)
  {
    if (m_current_scene->exit) { change_scene(); }
    m_current_scene->on_update(delta);
  }
  void dispatch_event(sf::Event ev) { m_current_scene->on_event(ev); }
  void render_scene(sf::RenderTarget &gfx) { m_current_scene->on_render(gfx); }

private:
  Game *m_game;
  std::unique_ptr<Scene> m_current_scene;
};
/*
Old implementation when experimenting with std::variant

template<SceneType... Scenes> struct SceneManager
{
  explicit SceneManager(Game &game) : m_game(&game) {}
  template<AnyOf<Scenes...> T> void change_scene()
  {
    assert(m_game != nullptr);
    m_current_scene = T();
    // m_current_scene.swap(T());
    std::visit(

      [this](auto &&val) {
        val.init(*m_game);
        val.on_init();
      },
      m_current_scene);
  }
  auto get_current_scene() const { return this->m_current_scene; }
  void update_scene(float delta)
  {
    std::visit(

      [this, &delta](auto &&val) {
        if (val.exit) {
          val.on_exit();
          change_scene<typename std::remove_reference_t<decltype(val)>::next>();
          return;
        }
        val.on_update(delta);
      },

      m_current_scene);
  }
  void dispatch_scene_event(sf::Event &ev)
  {
    std::visit([this, &ev](auto &&val) { val.on_event(ev); }, m_current_scene);
  }
  void render_scene(sf::RenderTarget &gfx)
  {
    std::visit([&gfx](auto &&val) { val.on_render(gfx); }, m_current_scene);
  }

private:
  Game *m_game;
  std::variant<Scenes...> m_current_scene;
};
*/
}// namespace netgame
