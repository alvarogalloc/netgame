#pragma once

#include "Interfaces/Game.h"
#include "Interfaces/InputApi.h"
#include <ranges>
namespace netgame {

class MyInputApi : public netgame::InputApi
{
  Game *game;
  std::vector<sf::Keyboard::Key> pressed_keys;
  std::vector<sf::Keyboard::Key> released_keys;
  std::vector<sf::Keyboard::Key> down_keys;
  std::vector<sf::Mouse::Button> pressed_mouse_buttons;
  std::vector<sf::Mouse::Button> released_mouse_buttons;
  std::vector<sf::Mouse::Button> down_mouse_buttons;
  sf::Vector2i mouse_position;

public:
  void update(const sf::Event &event);
  MyInputApi(Game &);
  [[nodiscard]] bool is_key_pressed(sf::Keyboard::Key key) const override
  {
    return std::ranges::find(pressed_keys, key) != pressed_keys.end();
  }
  [[nodiscard]] bool is_key_released(sf::Keyboard::Key key) const override
  {
    return std::ranges::find(released_keys, key) != released_keys.end();
  }
  [[nodiscard]] bool is_key_down(sf::Keyboard::Key key) const override
  {
    return std::ranges::find(down_keys, key) != down_keys.end();
  }
  [[nodiscard]] bool is_mouse_pressed(sf::Mouse::Button button) const override
  {
    return std::ranges::find(pressed_mouse_buttons, button) != pressed_mouse_buttons.end();
  }
  [[nodiscard]] bool is_mouse_released(sf::Mouse::Button button) const override
  {
    return std::ranges::find(released_mouse_buttons, button) != released_mouse_buttons.end();
  }
  [[nodiscard]] bool is_mouse_down(sf::Mouse::Button button) const override
  {
    return std::ranges::find(down_mouse_buttons, button) != down_mouse_buttons.end();
  }
  [[nodiscard]] sf::Vector2i get_mouse_position() const override
  {
    return mouse_position;
  }
};
}// namespace netgame
