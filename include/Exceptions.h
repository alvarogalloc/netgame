#pragma once
#include <exception>
#include <fmt/core.h>
#include <fmt/format.h>
#include <stdexcept>
#include <string_view>
#include <utility>


namespace netgame {
struct resource_error : std::runtime_error
{
  explicit resource_error(std::string_view asset_path)
    : std::runtime_error(fmt::format("Could not load asset: {}", asset_path))
  {}
};
struct path_error : std::runtime_error
{
  explicit path_error(std::string_view path)
    : std::runtime_error(fmt::format("Could not find path: {}", path))
  {}
};
struct animation_error : std::runtime_error
{
  explicit animation_error(std::string_view name)
    : std::runtime_error(fmt::format("Animation {} not found", name))
  {}
};
}// namespace netgame
