#pragma once

#include "GameState.h"
#include <SFML/Graphics/Texture.hpp>

// struct action {
//   sf::Event::EventType type;
//   sf::Keyboard::Key key;
//   sf::Event::Mosehj button;
// };

namespace netgame {
  entity create_player(netgame::GameState &state, sf::Texture &texture);
}// namespace netgame
