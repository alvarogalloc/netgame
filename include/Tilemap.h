#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/PrimitiveType.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <memory>
#include <span>

namespace netgame {
class Tilemap
  : public sf::Drawable
  , public sf::Transformable
{
public:
  bool create(std::shared_ptr<sf::Texture> tileset,
    sf::Vector2u tileSize,
    const std::span<std::uint8_t> tiles,
    std::uint32_t width,
    std::uint32_t height);

private:
  void draw(sf::RenderTarget &target,  sf::RenderStates states) const ;

  // store our geometry to draw tiles
  sf::VertexArray m_vertices;
  // Texture to our tileset
  std::shared_ptr<sf::Texture> m_tileset;
};

}// namespace netgame
