#pragma once

#include <sol/sol.hpp>
#include "Interfaces/InputApi.h"

namespace netgame {
void bins_input_api(sol::state& lua, InputApi& input_api){
  lua.new_enum("keys", 

    "A", sf::Keyboard::A,
    "B", sf::Keyboard::B,
               )
  lua["input"] = sol::table();
  lua["input"]["is_key_pressed"] = [&input_api](sf::Keyboard::Key key) {
    return input_api.is_key_pressed(key);
  }; 
  lua["input"]["is_key_released"] = [&input_api](sf::Keyboard::Key key) {
    return input_api.is_key_released(key);
  };
  lua["input"]["is_key_down"] = [&input_api](sf::Keyboard::Key key) {
    return input_api.is_key_down(key);
  };
  lua["input"]["is_mouse_pressed"] = [&input_api](sf::Mouse::Button button) {
    return input_api.is_mouse_pressed(button);
  };
  lua["input"]["is_mouse_released"] = [&input_api](sf::Mouse::Button button) {
    return input_api.is_mouse_released(button);
  };
  lua["input"]["is_mouse_down"] = [&input_api](sf::Mouse::Button button) {
    return input_api.is_mouse_down(button);
  };
  lua["input"]["get_mouse_position"] = [&input_api]() {
    return input_api.get_mouse_position();
  };
  
}

struct Script {
  static Script load(sol::state& lua);
  void run();
};
}
