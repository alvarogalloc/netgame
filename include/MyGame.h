
#pragma once
#include "AssetManager.h"
#include "GameState.h"
#include "Interfaces/Game.h"
#include "MyInputApi.h"
#include "SceneManager.h"
#include "scenes/StartScene.h"
#include <SFML/Window/VideoMode.hpp>
#include <memory>
#include <string_view>
namespace netgame {

class MyGame : public Game
{
public:
  MyGame(const MyGame &) = delete;
  MyGame(MyGame &&) = delete;
  MyGame &operator=(const MyGame &) = delete;
  MyGame &operator=(MyGame &&) = delete;
  explicit MyGame(std::string_view game_name);

  [[nodiscard]] bool running() const override;
  void update() override;
  void render() override;

  // implement interface functions
  sf::RenderWindow &get_window() override { return m_window; }
  AssetManager &get_asset_manager() override { return m_asset_manager; }
  SceneManager &get_scene_manager() override { return m_scene_manager; }
  sf::Clock &get_clock() override { return m_clock; }
  sf::VideoMode get_dimensions() const override { return m_dimensions; }
  GameState &get_state() override { return m_state; }
  InputApi &get_input_api() override { return m_input_api; };
  ~MyGame() noexcept override = default;

private:
  MyInputApi m_input_api;
  std::string_view m_game_name;
  sf::RenderWindow m_window;
  sf::VideoMode m_dimensions;
  AssetManager m_asset_manager;
  SceneManager m_scene_manager;
  sf::Clock m_clock;
  GameState m_state;
  // SceneManager<StartScene, GameScene> m_scene_manager;
};

}// namespace netgame
