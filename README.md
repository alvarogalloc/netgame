# Netgame (real name yet to come)

## Description
This is a personal experiment that aims to use c++ the way I like it to be used. The plan is to be a 2d platformer about a capybara. A list of things that I think that make a c++ human and modern:
1. At least c++ 17 standard (this project used c++ 20).
2. Readability and simpleness: clang-tidy checks and clang-format help on this.
3. Templates in a good way (not much nesting, concepts, using declarations for shortening, etc)
4. Responsable use of `auto`.
5. WIP: Good way of handling errors: Right now using exceptions and assertions.
6. Good libraries (using: spdlog, imgui, sfml, boost-ut & ginseng) and good use of the standard library.
7. Modern and CONCISE cmake, not worrying a lot about libraries and constraining (for now) to platforms and compilers I know that work. Note that I prefer premake, but it is way simple and is not as scalable for more complex projects.
8. Make the user feel like reading a higher level language, while having the best of the c++ performance.
9. WIP: move to c++ modules, starting to port the sfml wrapper to modules, but i don't like it much because of lack of intellisense.
10. Making the repo as small as possible.
11. A good package manager (vcpkg without manifest mode as default because of the above reason, rename _vcpkg.json to vcpkg.json to activate it).

## Build instructions
1. Install vcpkg (guide[here](https://vcpkg.io/en/getting-started.html)).
2. install dependencies. `vcpkg install sfml imgui imgui-sfml asio spdlog bext-ut` (for windows just add `--triplet:x64-windows` as only 64 bit build have been tested).
3. Configure & build with cmake (I use Ninja for speed, but you can use any): 
```
mkdir build
cmake .. -G Ninja -DCMAKE_TOOLCHAIN_FILE="path/to/your/vcpkg.cmake"
cmake --build .
```
4. Run the target `netgame`.

## Contribute
If you want to help, you can by sharing it with your friends and opening issues and merge requests. Some to-do things are:
- Separate the Game Code and the sdk into different projects.
- Scripting (I want to try [pocketpy (python)](https://pocketpy.dev/), but as it is immature yet, the best option is using [sol2 (lua)](https://sol2.readthedocs.io/en/latest/)).
- Fixing CI/CD as I can't get bothered about it, but it is key anyway.
- Integrating the ECS(ginseng) in a good and unintrusive way.

### Disclaimer
Please do not take my word as truth. This is the way I like it because I've adapted to small dev environments and like the things modern but fast and lightweight, which most times is almost impossible. 
