module;
#include <spdlog/spdlog.h>
export module spdlog;
export namespace spdlog {
using spdlog::debug;
using spdlog::info;
using spdlog::warn;
using spdlog::error;
}// namespace spdlog