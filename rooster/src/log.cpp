export module rooster.log;
import spdlog;

export namespace rooster {
using spdlog::debug;
using spdlog::info;
using spdlog::warn;
using spdlog::error;
}// namespace rooster
