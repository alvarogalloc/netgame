module;
#include <chrono>
#include <iostream>
#include <entt/entity/registry.hpp>
export module rooster.game;
import rooster.ecs;

export namespace rooster {
using registry = entt::registry;
using namespace std::chrono_literals;

constexpr std::chrono::nanoseconds timestep(16ms);

enum class gameflow { running, stop };
class game
{
private:
  using func_type = void(registry &);
  registry m_registry;
  hook<func_type> m_hook_setup;
  hook<func_type> m_hook_end;
  hook<func_type> m_hook_systems;
  gameflow m_gameflow;

public:
  using game_clock = std::chrono::steady_clock;
  game &add_setup_callback(std::function<func_type> func)
  {
    m_hook_setup.connect(func);
    return *this;
  }
  game &add_end_callback(std::function<func_type> func)
  {
    m_hook_end.connect(func);
    return *this;
  }
  game &add_system(std::function<func_type> func)
  {
    m_hook_systems.connect(func);
    return *this;
  }

  void run()
  {
    auto time_start = game_clock::now();
    m_gameflow = gameflow::running;
    m_hook_setup.publish(m_registry);
    m_registry.ctx().emplace<gameflow&>(m_gameflow);


    while (m_gameflow == gameflow::running) {
      auto time_delta = std::chrono::duration_cast<std::chrono::duration<float>>(game_clock::now() - time_start);
      time_start = time_start = game_clock::now();
      std::cout << "delta: " << time_delta.count() << '\n';
      m_hook_systems.publish(m_registry);
    }

    m_hook_end.publish(m_registry);
  }
};

}// namespace rooster
