SRC_FILES = $(shell find src/ -type f -name '*')
INCLUDE_FILES = $(shell find include/ -type f -name '*')
BUILD_FILES = $(shell find build/  -type f -name '*.ninja')

build: CMakeLists.txt $(SRC_FILES) $(INCLUDE_FILES) $(BUILD_FILES)
	@mkdir -p build
	@cmake -S . -B build -G "Ninja Multi-Config" -DCMAKE_TOOLCHAIN_FILE="~/vcpkg/scripts/buildsystems/vcpkg.cmake" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
	@cmake --build build


run: build
	./build/Debug/netgame

format:
	@clang-format -i src/**.cpp include/**.h --sort-includes

lint:
	@echo "Linting with cppcheck"
	@cppcheck --enable=all src

