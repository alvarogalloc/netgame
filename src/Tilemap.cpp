#include "Tilemap.h"
#include <algorithm>
#include <iostream>
#include <numeric>
#include <ranges>
#include <utility>


using namespace netgame;
// we will use a yaml specification with:
/*
- file of tileset: str 
- n rows : uint
- n columns : uint
- tilesize : vec2<uint>
- layers:
  - layer1: array of uints with size n_rows * n_columns
  - layern: array of uints with size n_rows * n_columns
*/

bool Tilemap::create(std::shared_ptr<sf::Texture> tileset,
  const sf::Vector2u tileSize,
  const std::span<std::uint8_t> tiles,
  const std::uint32_t width,
  const std::uint32_t height)
{
  if (tileset == nullptr) { return false; }
  // load the tileset texture
  m_tileset = tileset;

  // resize the vertex array to fit the level size
  m_vertices.setPrimitiveType(sf::PrimitiveType::Triangles);
  m_vertices.resize(
    static_cast<std::size_t>(width) * static_cast<std::size_t>(height));

  // populate the vertex array, with one  per tile
  for (auto i : std::views::iota(std::uint32_t(0), width))
  {
    for (auto j : std::views::iota(std::uint32_t(0), height))
      std::cout << i << '\n';
  }
  return true;
}


void Tilemap::draw(sf::RenderTarget &target,
  sf::RenderStates states) const
{
  // sf::RenderStates new_states{
  //   states.blendMode, getTransform(), this->m_tileset.get(), states.shader
  // };
  //
  // // draw the vertex array
  // target.draw(m_vertices, new_states);
}
