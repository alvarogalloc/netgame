#include "AssetManager.h"
#include <algorithm>
#include <iostream>

using namespace netgame;

AssetManager::AssetManager(std::string_view _asset_path)
  : asset_path(_asset_path)
{
  namespace fs = std::filesystem;
  if (!fs::exists(asset_path)) { throw netgame::path_error{ asset_path }; }
  if (!asset_path.ends_with('/')) { asset_path += '/'; }
}
