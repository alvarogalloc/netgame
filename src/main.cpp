#include "Exceptions.h"
#include "MyGame.h"
#include "scenes/StartScene.h"
#include <exception>
#include <iostream>
#include <memory>
#include <spdlog/spdlog.h>


int main(int /*argc*/, char * /*argv*/[])
{
  try
  {
    netgame::MyGame game("capy");
    while (game.running())
    {
      game.update();
      game.render();
    }
  } catch (const netgame::path_error &e)
  {
    spdlog::error(
      "[netgame::path_error]: {}", e.what());
    return -1;
  } 
  catch (const netgame::resource_error &e)
  {
    spdlog::error(
      "[netgame::resource_error]: {}", e.what());
    return -1;
  } catch (const std::exception &e)
  {
    spdlog::error("Handled standard exception, msg: {}", e.what());
  } catch (...)
  {
    std::cout << "exception unhandled" << '\n';
  }
  spdlog::info("Exiting...");
}
