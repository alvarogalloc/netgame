#include "GameState.h"
#include "AssetManager.h"
#include "Interfaces/Game.h"
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System/Vector2.hpp>

namespace netgame {
GameState::GameState(Game *game) // NOLINT
{
  // db.ctx().emplace<sf::RenderWindow &>(game->get_window());
  // db.ctx().emplace<AssetManager &>(game->get_asset_manager());
  // db.ctx().emplace<SceneManager &>(game->get_scene_manager());
  // db.ctx().emplace<sf::Clock &>(game->get_clock());
}
void GameState::update(float delta)
{
  auto animview = db.view<netgame::Animator, sf::Vector2f>();
  animview.each([&](netgame::Animator &animation, sf::Vector2f &vel) {
    animation.update(delta);
    animation.get_sprite().move(vel * delta);
  });
}
void GameState::render(sf::RenderTarget &target)
{
}

}// namespace netgame
