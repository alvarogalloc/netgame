#include "Animation.h"
#include "AssetManager.h"
#include "Interfaces/Game.h"
#include <cmath>
#include <scenes/GameScene.h>
#include <scenes/StartScene.h>
#include <spdlog/spdlog.h>

using namespace netgame;
using sf::Font;


// called when changing scenes
void StartScene::on_init()
{
  // START LOADING
  music = m_assets->get<sf::Music>("music/piano.ogg");
  auto bg_texture = m_assets->get<sf::Texture>("bg.png");
  auto capybara_texture = m_assets->get<sf::Texture>("capybara.png");
  auto font = m_assets->get<sf::Font>("BitPotion.ttf");
  // END LOADING

  music->play();

  const sf::Vector2f dimensions_float{ float(m_game->get_dimensions().width),
    float(m_game->get_dimensions().height) };
  // Background
  m_bg_sprite.setTexture(*bg_texture);
  // scale to screen size
  m_bg_sprite.setScale(
    { dimensions_float.x / float(m_bg_sprite.getTexture()->getSize().x),
      dimensions_float.y / float(m_bg_sprite.getTexture()->getSize().y) });

  // init title text
  const std::uint8_t char_size = 180;
  const sf::Color text_color(sf::Color::White);
  auto center_origin = [](sf::Text &text) {
    const sf::Vector2f text_size{ text.getLocalBounds().width,
      text.getLocalBounds().height };
    text.setOrigin(text_size / 2.f);// NOLINT
  };
  const sf::Vector2f title_pos = { dimensions_float.x / 2, 50 };
  m_title_text = std::make_unique<sf::Text>();
  m_title_text->setFont(*font);
  m_title_text->setString("Capybaras");
  m_title_text->setPosition(title_pos);
  m_title_text->setFillColor(text_color);
  m_title_text->setCharacterSize(char_size);
  center_origin(*m_title_text);

  m_start_text = std::make_unique<sf::Text>();
  m_start_text->setFont(*font);
  m_start_text->setString("Press any key to start");
  const sf::Vector2f start_text_offset{ 0, 480 };
  m_start_text->setPosition(title_pos + start_text_offset);
  m_start_text->setFillColor(text_color);
  const float start_text_scale = 0.7;
  m_start_text->setCharacterSize(std::lround(char_size * start_text_scale));
  center_origin(*m_start_text);

  const sf::Vector2i cellsize{ 64, 64 };
  const sf::Vector2u sheet_start{ 0, 64 * 8 };
  const std::uint8_t n_frames = 8;
  const float duration{ 1.0 };
  // set capybara Animation
  // m_capy_walk_desc =
  //   AnimationDescription{ .m_spritesheet = capybara_texture.get(),
  //     .pos_size = { sheet_start, cellsize },
  //     .n_frames = n_frames,
  //     .duration = duration };
  // m_capy_walk = Animation(m_capy_walk_desc);
  m_capy_walk = Animator(capybara_texture.get(), { 9, 9 });
  m_capy_walk.add_animation("walk", { 0, 1 }, { 7, 1 }, 0.5, true);
  // positioning sprite
  auto center_to_screen = [&dimensions_float](const sf::Vector2f size) {
    sf::Vector2f ret;
    const float scale = 0.5f;
    ret.x = dimensions_float.x * scale - size.x * scale;
    ret.y = dimensions_float.y * scale - size.y * scale;
    return ret;
  };
  auto &sprite = m_capy_walk.get_sprite();
  sf::Vector2f scale{ 3, 3 };
  sprite.setScale(scale);
  // get actual size of sprite with scale & stuff
  // sf::Vector2f sprite_size{
  // static_cast<float>(sprite.getGlobalBounds().width),
  //   static_cast<float>(sprite.getGlobalBounds().height) };
  // sprite.setPosition(center_to_screen(sprite_size));
  sprite.move(sf::Vector2f{ 0, 64 * scale.y });// NOLINT
  m_capy_walk.play("walk");

  spdlog::info(
    "sprite pos: {},{}", sprite.getPosition().x, sprite.getPosition().y);
}

void StartScene::on_update(float delta)
{
  m_capy_walk.update(delta);
  const std::uint8_t blend_max_turning_point = 253;
  const std::uint8_t blend_min_turning_point = 2;
  auto blend_text = [this](sf::Text &text) {
    const auto color = text.getFillColor();
    if (color.a <= blend_min_turning_point)
      is_blending_up = true;
    else if (color.a >= blend_max_turning_point)
      is_blending_up = false;
    auto newcolor = text.getFillColor();
    if (is_blending_up)
      newcolor.a += 2;
    else
      newcolor.a -= 2;
    text.setFillColor(newcolor);
  };
  blend_text(*m_start_text);
  blend_text(*m_title_text);
}

void StartScene::on_event(sf::Event &ev)
{
  if (ev.type == sf::Event::KeyPressed)
  {
    // NOLINTNEXTLINE
    if (ev.key.code == sf::Keyboard::N) { exit = true; }
  }
}

void StartScene::on_render(sf::RenderTarget &gfx)
{
  // gfx.clear(sf::Color::Green);
  gfx.draw(m_bg_sprite);
  gfx.draw(m_capy_walk);
  gfx.draw(*m_title_text);
  gfx.draw(*m_start_text);
}

std::unique_ptr<Scene> StartScene::on_exit()
{
  music->stop();
  return std::make_unique<GameScene>();
}
