#include "scenes/GameScene.h"
#include "AssetManager.h"
#include "Interfaces/Game.h"
#include "SFML/Graphics/Texture.hpp"
#include "entities/Prefabs.h"
#include "scenes/StartScene.h"
#include <SFML/Graphics/RectangleShape.hpp>
#include <iostream>
#include <thread>

using namespace netgame;

void GameScene::on_init()
{
  m_player = create_player(
    m_game->get_state(), *m_assets->get<sf::Texture>("capybara.png"));
}
void GameScene::on_update(float /*delta*/)
{
  // move player with wasd
  const float speed = 150;
  // get player velocity
  auto &vel = m_state->db.get<sf::Vector2f>(m_player);
  auto &animation = m_state->db.get<Animator>(m_player);
  auto& input = m_game->get_input_api();
  if (input.is_key_pressed(sf::Keyboard::W)) { vel.y = -speed; }

  // if its moving, play the animation
  if (vel.x != 0 || vel.y != 0)
  {
    if (!animation.playing()) animation.play("walk");
  } else
  {
    animation.pause();
  }

  if (input.is_key_pressed(sf::Keyboard::D)
      && !input.is_key_pressed(sf::Keyboard::A))
  {
    vel.x = speed;
  }
  if (input.is_key_pressed(sf::Keyboard::A)
      && !input.is_key_pressed(sf::Keyboard::D))
  {
    vel.x = -speed;
  }
  // flip excluding keys one another
  // if (m_game->is_key_pressed(sf::Keyboard::A)
  //     && !m_game->is_key_pressed(sf::Keyboard::D))
  // {
  //   animation.set_flipped(true);
  // }
  // if (m_game->is_key_pressed(sf::Keyboard::D)
  //     && !m_game->is_key_pressed(sf::Keyboard::A))
  // {
  //   animation.set_flipped(false);
  // }
}

void GameScene::on_event(sf::Event &ev)
{
  if (ev.key.code == sf::Keyboard::Space) { exit = true; }// NOLINT
  if (ev.type == sf::Event::KeyReleased)
  {
    auto &vel = m_state->db.get<sf::Vector2f>(m_player);
    if (ev.key.code == sf::Keyboard::W// NOLINT
        || ev.key.code == sf::Keyboard::S)// NOLINT
      vel.y = 0;
    if (ev.key.code == sf::Keyboard::A// NOLINT
        || ev.key.code == sf::Keyboard::D)// NOLINT
      vel.x = 0;
  }
}

void GameScene::on_render(sf::RenderTarget &gfx)
{
  gfx.clear();
  // around capybara
  sf::RectangleShape rect;

  rect.setSize(sf::Vector2f{ 64 * 3, 64 * 3 });
  rect.setFillColor(sf::Color::Transparent);
  rect.setOutlineColor(sf::Color::Red);
  rect.setOutlineThickness(1);
  rect.setPosition(
    m_state->db.get<Animator>(m_player).get_sprite().getPosition());
  gfx.draw(rect);
}

std::unique_ptr<Scene> GameScene::on_exit()
{
  m_state->delete_entity(m_player);
  return std::make_unique<StartScene>();
}
