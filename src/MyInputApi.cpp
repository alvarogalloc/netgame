#include "MyInputApi.h"


namespace netgame {
MyInputApi::MyInputApi(Game &game) : game(&game) {}

void MyInputApi::update(const sf::Event &event)
{
  pressed_keys.clear();
  released_keys.clear();
  pressed_mouse_buttons.clear();
  released_mouse_buttons.clear();

  switch (event.type)
  {
  case sf::Event::KeyPressed:
    pressed_keys.push_back(event.key.code);
    down_keys.push_back(event.key.code);
    break;
  case sf::Event::KeyReleased:
    released_keys.push_back(event.key.code);
    down_keys.erase(
      std::remove(down_keys.begin(), down_keys.end(), event.key.code),
      down_keys.end());
    break;
  case sf::Event::MouseButtonPressed:
    pressed_mouse_buttons.push_back(event.mouseButton.button);
    down_mouse_buttons.push_back(event.mouseButton.button);
    break;
  case sf::Event::MouseButtonReleased:
    released_mouse_buttons.push_back(event.mouseButton.button);
    down_mouse_buttons.erase(std::remove(down_mouse_buttons.begin(),
                               down_mouse_buttons.end(),
                               event.mouseButton.button),
      down_mouse_buttons.end());
    break;
  case sf::Event::MouseMoved:
    mouse_position = sf::Vector2i(event.mouseMove.x, event.mouseMove.y);
    break;
  default:
    break;
  }
}

}// namespace netgame
