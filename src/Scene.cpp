#include "Scene.h"
#include "Interfaces/Game.h"
#include <cassert>
namespace netgame {

void Scene::init(Game *game)
{
  if (game == nullptr)
  {
    throw std::invalid_argument{ "You cannot pass an empty game" };
  }
  m_game = game;
  assert(m_game != nullptr);
  m_assets = &game->get_asset_manager();
  assert(m_assets != nullptr);
  m_state = &game->get_state();
  assert(m_state != nullptr);
}
}// namespace netgame
