#include "entities/Prefabs.h"
#include "Animation.h"
netgame::entity netgame::create_player(netgame::GameState &state,
  sf::Texture &texture)
{
  using namespace netgame;

  auto id = state.db.create();
  // state.db.add_component(id, component::no_render_tag{});
  // add velocity component
  state.db.emplace<sf::Vector2f>(id, 0, 0);
  // add animation component
  const sf::Vector2i cellsize{ 64, 64 };
  const sf::Vector2i sheet_start{ 0, 64 * 8 };
  const std::uint8_t n_frames = 8;
  const float duration{ 0.2 };
  Animator animator(&texture, { 9, 9 });
  animator.add_animation("walk", { 0, 1 }, { 7, 1 }, 1, true);
  animator.play("walk");
  state.db.emplace<Animator>(id, std::move(animator));
  auto &sprite = state.db.get<Animator>(id).get_sprite();
  const sf::Vector2f scale{ 3, 3 };
  sprite.setScale(scale);
  // get actual size of sprite with scale & stuff
  sf::Vector2f sprite_size{ static_cast<float>(sprite.getGlobalBounds().width),
    static_cast<float>(sprite.getGlobalBounds().height) };
  sprite.move(sf::Vector2f{ 0, 64 * scale.y });// NOLINT
  return id;
}
