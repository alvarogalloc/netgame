#include "MyGame.h"
#include "Interfaces/Game.h"
#include "scenes/StartScene.h"
// #include <imgui-SFML.h>
// #include <imgui.h>
#include <memory>
#include <spdlog/spdlog.h>
#include <utility>

using namespace netgame;
const sf::VideoMode window_size{ 1280, 720 };

MyGame::MyGame(std::string_view game_name)
  : m_scene_manager(this, std::make_unique<StartScene>()),
    m_dimensions{ window_size }, m_asset_manager("./assets"), m_state(this),
    m_game_name(game_name), m_input_api(*this)
{
  m_window.create(m_dimensions, game_name.data(), sf::Style::Close);
  m_window.setVerticalSyncEnabled(true);
  const auto [win_width, win_heigth] = m_window.getSize();
  spdlog::info("Window created with size: x {} y {}", win_width, win_heigth);
  // if (!ImGui::SFML::Init(m_window, true))
  // {
  //   spdlog::error("Couldn't initialize imgui\n");
  // }
  // preload the font that we'll use
  // spdlog::info("Preload Game font");
  // m_assets.load("BitPotion.ttf", NewAssetManager::AssetType::Font);
  // spdlog::info("Done Game font");
  // ImGuiIO &io = ImGui::GetIO();
  const float fontsize = 48.f;
  // auto *font = io.Fonts->AddFontFromFileTTF("assets/BitPotion.ttf",
  // fontsize); io.FontDefault = font; io.Fonts->Build(); if
  // (!ImGui::SFML::UpdateFontTexture())
  // {
  //   spdlog::error("Couldn't update font texture imgui sfml");
  // }
  m_scene_manager.init();
}
bool MyGame::running() const
{
  // if window is open and there is a scene playing
  return m_window.isOpen();
}
void MyGame::update()
{
  sf::Event ev{};
  while (m_window.pollEvent(ev))
  {
    // ImGui::SFML::ProcessEvent(ev);
    if (ev.type == sf::Event::Closed) { m_window.close(); }
    m_scene_manager.dispatch_event(ev);
    m_input_api.update(ev);
  }
  // ImGui::SFML::Update(m_window, m_clock.getElapsedTime());
  m_state.update(m_clock.getElapsedTime().asSeconds());
  m_scene_manager.update(m_clock.restart().asSeconds());
}

void MyGame::render()
{
  m_window.clear();
  m_scene_manager.render_scene(m_window);
  m_state.render(m_window);
  // ImGui::SFML::Render(m_window);
  m_window.display();
}

