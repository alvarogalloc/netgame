#include "Animation.h"
#include "Exceptions.h"
#include <cassert>
#include <spdlog/spdlog.h>

void netgame::Animator::set_frame(const sf::Vector2u coords)
{
  const auto texture_size = m_sprite.getTexture()->getSize();
  const auto frame_size = sf::Vector2f(
    texture_size.x / m_tileset_size.x, texture_size.y / m_tileset_size.y);
  const auto rect = sf::IntRect(coords.x * frame_size.x,
    coords.y * frame_size.y,
    frame_size.x,
    frame_size.y);
  spdlog::info("setting frame to {},{} with size {},{}",
    rect.left,
    rect.top,
    rect.width,
    rect.height);
  m_sprite.setTextureRect(rect);
  m_curr_frame = coords;
}

netgame::Animator::Animator(sf::Texture *spritesheet, sf::Vector2u tileset_size)
  : m_spritesheet(spritesheet), m_tileset_size(tileset_size), m_curr_animation(m_animations.end())
{
  assert(m_spritesheet != nullptr);
  m_sprite.setTexture(*m_spritesheet);
}

void netgame::Animator::add_animation(std::string_view name,
  sf::Vector2u start_frame,
  sf::Vector2u end_frame,
  float duration,
  bool repeat)
{
  m_animations.push_back({ name, start_frame, end_frame, duration, repeat });
}

void netgame::Animator::play(std::string_view name)
{
  // find the animation with the given name
  auto it = std::find_if(m_animations.begin(),
    m_animations.end(),
    [name](const auto &anim) { return anim.name == name; });
  // if (it == m_curr_animation) { return; }
  if (it == m_animations.end())
  {
    spdlog::error("Animation {} not found", name);
    return;
  }
  
  m_curr_animation = it;
  set_frame(it->start_frame);
  m_paused = false;
}
void netgame::Animator::update(float delta)
{
  // if the animation is paused or there is no current animation, do nothing
  if (m_paused || m_curr_animation == m_animations.end()) { return; }
  m_progress += delta;
  // first case: the animation finished
  if (m_progress >= m_curr_animation->duration)
  {
    spdlog::info("end of animation {}", m_curr_animation->name);
    m_progress = 0;
    if (m_curr_animation->repeat)
      replay();
    else
      m_paused = true;
    return;
  }

  // second case: the animation is still going
  const auto n_frames =
    m_curr_animation->end_frame.x - m_curr_animation->start_frame.x;
  const auto frame_duration = m_curr_animation->duration / n_frames;
  const auto time_to_step = m_curr_frame.x * frame_duration;
  if (m_progress >= time_to_step && m_curr_frame.x < n_frames)
  {

    // go to the next frame
    set_frame({ ++m_curr_frame.x, m_curr_frame.y });
  }
}
void netgame::Animator::replay()
{
  m_progress = 0;
  set_frame(m_curr_animation->start_frame);
}

namespace netgame::old {
using namespace netgame;

Animation::Animation(AnimationDescription description)
  : m_description(description)
{
  assert(m_description.m_spritesheet != nullptr);
  auto [x, y] = m_description.m_spritesheet->getSize();
  m_sprite.setTexture(*m_description.m_spritesheet);
  m_sprite.setTextureRect(description.pos_size);
}

void Animation::reset()
{
  auto rect = m_description.pos_size;
  rect.left = m_flipped
                ? m_description.pos_size.left + m_description.pos_size.width
                : m_description.pos_size.left;
  rect.width =
    m_flipped ? -m_description.pos_size.width : m_description.pos_size.width;
  m_description.progress = 0;
  m_description.curr_frame = 0;
  m_sprite.setTextureRect(rect);
}

void Animation::nextFrame()
{
  auto rect = m_description.pos_size;
  rect.left =
    m_flipped ? m_description.pos_size.left
                  + (m_description.pos_size.width * m_description.curr_frame++)
                  + m_description.pos_size.width
              : m_description.pos_size.left
                  + (m_description.pos_size.width * m_description.curr_frame++);
  rect.width =
    m_flipped ? -m_description.pos_size.width : m_description.pos_size.width;
  m_sprite.setTextureRect(rect);
}

void Animation::update(float delta)
{
  if (!m_is_playing) { return; }
  auto frame_duration = m_description.duration / float(m_description.n_frames);
  m_description.progress += delta;

  // if the animation finished, reset it
  if (m_description.progress >= m_description.duration) reset();
  // if there is a step
  else if (m_description.progress
           >= frame_duration * float(m_description.curr_frame))
    nextFrame();
}

void Animation::set_flipped(bool is_flipped)
{
  if (is_flipped == m_flipped) { return; }
  m_flipped = is_flipped;
  // take account of the current scale, position and size of sprite and the
  // texture rect.
  auto scale = m_sprite.getScale();
  if (m_flipped)
  {
    auto current_rect = m_sprite.getTextureRect();
    current_rect.width = -current_rect.width;
    m_sprite.setTextureRect(current_rect);
    // m_sprite.setScale({ -scale.x, scale.y });
    // move the sprite
  } else
  {
    auto current_rect = m_sprite.getTextureRect();
    current_rect.width = std::abs(current_rect.width);
    m_sprite.setTextureRect(current_rect);
  }
}
}// namespace netgame::old
